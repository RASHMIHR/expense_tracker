//
//  ViewModel.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift
import ObjectMapper
import UIKit

class ViewModel {
    var name = BehaviorRelay<String>(value: "")
    var amount = BehaviorRelay<String>(value: "0")
    var expense = BehaviorRelay<[DetailModel]>(value: [DetailModel]())
    var monthDetails = BehaviorRelay<[String:Int]>(value: [:])
    var expenses:[DetailModel] = []
    var detail:[String:Int] = [:]
    var userRepository: UserRepository!
    
    init(userRepository: UserRepository) {
        self.userRepository = userRepository
    }
    
    func addExpense(name: String, time: Int, amount: Int) {
        userRepository.addexpense(name: name, enterTime: time, amount: amount)
        fetchAll()
        fetch()
    }
    
    func fetchAll() {
        expenses.removeAll()
        let value = userRepository.fetchAllData()
        expenses.append(contentsOf: value)
        expense.accept(value)
        fetch()
    }
    
    func fetch() {
        detail.removeAll()
        monthDetails.accept(detail)
        var array:[String] = []
        let date = Date()
        let dateFormatter = DateFormatter()
        var dec = 0
        for val in 0...11 {
            if let cal = Calendar.current.date(byAdding: .month, value: dec, to: Date()) {
                dateFormatter.dateFormat = "MMM YYYY"
                let month = dateFormatter.string(from: cal as Date)
                array.append(month)
                detail[month] = 0
            }
            print(detail)
            dec = dec - 1
        }
        if expenses.count != 0{
        for i in 0...expenses.count - 1{
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale // edited
            dateFormatter.dateFormat = "MMM YYYY"
            let dateString = dateFormatter.string(from: date as Date)
            print(dateString)
            detail.forEach {key,val in
                if key ==  dateString {
                    let value = val + expenses[i].amount
                    detail[key] = value
                }
            }
        }
        }
        monthDetails.accept(detail)
    }
    
}

