//
//  DependencyInjector.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import Swinject
import CoreLocation

class DependencyInjector {
    
    private var container = Container()
    
    public static var defaultInjector: DependencyInjector = {
        let injector = DependencyInjector()
        return injector
    }()
    
    func getContainer()->Container{
        return container
    }
    
    func reset(){
        container = Container()
        register()
    }
    
    public func register() {
        container.register(UserRepository.self) {r in
            UserRepository()
        }.inObjectScope(ObjectScope.container)
        
        container.register(ViewModel.self) {r in
            ViewModel(userRepository: r.resolve(UserRepository.self)!)
        }.inObjectScope(ObjectScope.container)
        
    }
}


