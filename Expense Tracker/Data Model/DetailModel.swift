//
//  DetailModel.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import RealmSwift
import ObjectMapper

class DetailModel: Object, Mappable {
    
    var enterTime = RealmOptional<Int>()
    required convenience init?(map: Map) {
        self.init()
    }
    
    @objc dynamic var name: String?
    @objc dynamic var amount = 0
    
    func mapping(map: Map) {
        enterTime <- map["Date"]
        name <- map["name"]
        amount <- map["amount"]
    }
    
    override static func primaryKey() -> String? {
        return "enterTime"
    }
    
}
