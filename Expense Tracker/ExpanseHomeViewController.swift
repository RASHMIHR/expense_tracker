//
//  ExpanseHomeViewController.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import UIKit
import RxSwift

class ExpanseHomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addExpenseButton: UIButton!
    @IBOutlet weak var expenseSummary: UIButton!
    @IBOutlet weak var monthTotalLabel: UILabel!
    private var viewModel: ViewModel!
    var disposeBag = DisposeBag()
    var sum = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = DependencyInjector.defaultInjector.getContainer().resolve(ViewModel.self)
        viewModel.fetchAll()
        tableView.register(UINib(nibName: "ExpenseTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpenseTableViewCell")
        addExpenseButton.layer.cornerRadius = 5
        expenseSummary.layer.cornerRadius = 5
        viewModel.expense.asObservable().bind(to: tableView.rx.items(cellIdentifier: "ExpenseTableViewCell")) { index, modal, cell in
            let cell = cell as! ExpenseTableViewCell
            cell.configure(model: modal)
            
        }.disposed(by: self.disposeBag)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.setNeedsDisplay()
        var sum = 0
        if viewModel.expenses.count != 0{
        for i in 0...viewModel.expenses.count - 1 {
            let value = viewModel.expenses[i].amount
            sum = sum + value
            monthTotalLabel.text = String(sum)
        }
        }
    }
    
    @IBAction func addNewExpenseButtonTap(_ sender: Any) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddNewExpenseViewController") as? AddNewExpenseViewController {
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = UIColor.init(white: 0.1, alpha: 0.9)
            self.navigationController?.present(vc, animated: true)
        }
    }
    
    @IBAction func expenseSummaryButtonTap(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(identifier: "ExpenseSummaryViewController") as? ExpenseSummaryViewController {
            self.navigationController?.present(vc, animated: true)
        }
    }
}
