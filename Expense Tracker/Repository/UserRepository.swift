//
//  UserRepository.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import RealmSwift
import ObjectMapper

class UserRepository {
    
    func addexpense(name: String, enterTime: Int, amount: Int) { // to add new expense data into database
        let expense = DetailModel()
        let realm = try! Realm()
        try! realm.write {
            expense.name = name
            expense.enterTime.value = enterTime
            expense.amount = amount
            realm.add(expense, update: .all)
        }
    }
    
    func fetchAllData() -> [DetailModel] { //Retrieving  all the data from Database
        var expenses: [DetailModel] = []
        let realm = try! Realm()
        let saveItems = realm.objects(DetailModel.self)
        if saveItems.count != 0{
            for expense in 0...saveItems.count - 1{
                expenses.append(saveItems[expense])
            }
        }
        return expenses
    }
    
    func fetch() -> [DetailModel] {
        var expenses: [DetailModel] = []
        let realm = try! Realm()
        let items = realm.objects(DetailModel.self)
        let startDate = Date()
        let lastDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        return expenses
    }
    
}
