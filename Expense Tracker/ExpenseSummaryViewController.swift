//
//  ExpenseSummaryViewController.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import UIKit
import RxSwift

class ExpenseSummaryViewController: UIViewController {
    
    @IBOutlet weak var totalNabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: ViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = DependencyInjector.defaultInjector.getContainer().resolve(ViewModel.self)
        viewModel.monthDetails.asObservable().bind(to: tableView.rx.items(cellIdentifier: "DetailMonthTableViewCell")) { index, modal, cell in
            let cell = cell as! DetailMonthTableViewCell
            cell.monthLabel.text = modal.key
            cell.amountLabel.text = String(modal.value)
        }.disposed(by: self.disposeBag)
        var sum = 0
        for i in 0...viewModel.expenses.count - 1 {
            let value = viewModel.expenses[i].amount
            sum = sum + value
            totalNabel.text = String(sum)
        }
    }
    
    @IBAction func closeButtonTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
