//
//  ExpenseTableViewCell.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var expenseName: UILabel!
    @IBOutlet weak var expenseAmount: UILabel!
    @IBOutlet weak var displayImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.cornerRadius = 5
        contentView.backgroundColor = UIColor.white
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }

    
    func configure(model: DetailModel) {
        if let time = model.enterTime.value{
            //            let date = NSDate(timeIntervalSince1970: TimeInterval(time)/1000)
            let date = Date(timeIntervalSince1970: TimeInterval(time)/1000)
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale // edited
            dateFormatter.dateFormat = "MMM dd"
            let dateString = dateFormatter.string(from: date as Date)
            print("----->\(dateString)")
            dateLabel.text = dateString
        }
        expenseName.text = model.name
        expenseAmount.text = String(model.amount)
        if model.name?.uppercased() == "GAS" {
            displayImage.image = UIImage(named: "gas")
        }else if model.name?.uppercased() == "PARTY" {
            displayImage.image = UIImage(named: "party")
        }else if model.name?.uppercased() == "PIZZA" {
            displayImage.image = UIImage(named: "pizza")
        }else if model.name?.uppercased() == "FUEL" {
            displayImage.image = UIImage(named: "fuel")
        }else if model.name?.uppercased() == "FOOD" {
            displayImage.image = UIImage(named: "food")
        }else if model.name?.uppercased() == "EXPENSE" {
            displayImage.image = UIImage(named: "expense")
        }
    }
    
}
