//
//  AddNewExpenseViewController.swift
//  Expense Tracker
//
//  Created by Rashmi HR on 27/06/21.
//

import Foundation
import UIKit
import RxSwift

class AddNewExpenseViewController: UIViewController {
    
    @IBOutlet weak var expenseNameTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var addNewExpenseView: UIView!
    private var viewModel: ViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = DependencyInjector.defaultInjector.getContainer().resolve(ViewModel.self)
        saveButton.layer.cornerRadius = 5
        addNewExpenseView.layer.cornerRadius = 5
        expenseNameTextField.rx.text.orEmpty.bind(to: viewModel.name).disposed(by: disposeBag)
        amountTextField.rx.text.orEmpty.bind(to: viewModel.amount).disposed(by: disposeBag)
    }
    
    @IBAction func saveButtonTap(_ sender: Any) {
        let name = viewModel.name.value
        let amount = Int(viewModel.amount.value)
        let time = Int(NSDate().timeIntervalSince1970 * 1000)
        if let amount = amount {
        viewModel.addExpense(name: name, time: time, amount: amount)
        }
            dismiss(animated: true, completion: nil)
    }
    
}
